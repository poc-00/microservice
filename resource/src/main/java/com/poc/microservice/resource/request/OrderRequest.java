package com.poc.microservice.resource.request;

import com.poc.microservice.resource.dto.OrderDto;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderRequest {
    OrderDto data;
}
