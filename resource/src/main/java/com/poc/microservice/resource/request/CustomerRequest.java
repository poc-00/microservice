package com.poc.microservice.resource.request;

import com.poc.microservice.resource.dto.CustomerDto;
import lombok.Data;

@Data
public class CustomerRequest {
    CustomerDto request;
}
