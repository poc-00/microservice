package com.poc.microservice.resource.controllers;

import com.poc.microservice.resource.feignservice.CustomerServiceFeign;
import com.poc.microservice.resource.request.CustomerRequest;
import com.poc.microservice.resource.response.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api-customer")
public class CustomerResourceController {

    @Autowired
    private CustomerServiceFeign customerServiceFeign;

    @GetMapping("account-detail/{accountId}")
    public String getAccountDetail(@PathVariable int accountId) {
        return customerServiceFeign.getAccountDetail(accountId);
    }

    @PostMapping("create")
    public ResponseEntity<CustomerResponse> createCustomer(@RequestBody CustomerRequest customerRequest) {
        return customerServiceFeign.createCustomer(customerRequest);
    }
}
