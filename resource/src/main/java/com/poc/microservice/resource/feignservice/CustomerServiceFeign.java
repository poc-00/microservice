package com.poc.microservice.resource.feignservice;

import com.poc.microservice.resource.request.CustomerRequest;
import com.poc.microservice.resource.response.CustomerResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("customer-service")
public interface CustomerServiceFeign {
    @GetMapping("/app/customer/account-detail/{accountId}")
    String getAccountDetail(@PathVariable int accountId);

    @PostMapping("/app/customer/create")
    ResponseEntity<CustomerResponse> createCustomer(@RequestBody CustomerRequest customerRequest);
}
