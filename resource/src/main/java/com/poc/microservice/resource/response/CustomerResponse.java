package com.poc.microservice.resource.response;

import com.poc.microservice.resource.dto.CustomerDto;
import lombok.Data;

@Data
public class CustomerResponse {
    CustomerDto response;
}
