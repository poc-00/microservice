package com.poc.microservice.resource.dto;

import com.poc.microservice.common.enums.DomainEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderDto {
    int id;
    int customerId;
    double orderTotal;
    DomainEvent state;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", orderTotal=" + orderTotal +
                ", state=" + state +
                '}';
    }
}
