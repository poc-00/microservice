package com.poc.microservice.resource.controllers;

import com.poc.microservice.resource.feignservice.OrderServiceFeign;
import com.poc.microservice.resource.request.OrderRequest;
import com.poc.microservice.resource.response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api-order")
public class OrderResourceController {

    @Autowired
    private OrderServiceFeign orderServiceFeign;

    @GetMapping("user-account/{accountNumber}")
    public String getUserAccountDetail(@PathVariable int accountNumber) {
        return orderServiceFeign.getUserAccountDetail(accountNumber);
    }

    @PostMapping("create")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequest orderRequest) {
        return orderServiceFeign.createOrder(orderRequest);
    }
}
