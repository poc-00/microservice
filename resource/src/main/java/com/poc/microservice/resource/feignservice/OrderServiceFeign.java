package com.poc.microservice.resource.feignservice;

import com.poc.microservice.resource.request.OrderRequest;
import com.poc.microservice.resource.response.OrderResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("order-service")
public interface OrderServiceFeign {
    @GetMapping("/app/order/user-account/{accountNumber}")
    String getUserAccountDetail(@PathVariable int accountNumber);

    @PostMapping("/app/order/create")
    ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequest orderRequest);
}
