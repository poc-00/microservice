package com.poc.microservice.resource.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDto {
    private int id;
    private String name;
    private double creditLimit;

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creditLimit=" + creditLimit +
                '}';
    }
}
