package com.poc.microservice.resource.response;

import com.poc.microservice.resource.dto.OrderDto;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderResponse {
    OrderDto data;
}
